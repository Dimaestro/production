import { Button } from '@/components/ui/button'

export default function Home() {
    return (
        <main className="flex min-h-screen flex-col items-center justify-between grow">
            <Button>Button</Button>
        </main>
    )
}
